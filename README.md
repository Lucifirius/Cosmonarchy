# Cosmonarchy BW

Cosmonarchy BW, a mod for Starcraft version 1.16.1, is the prototype of the definitive RTS experience.

Please send any feedback to me through [my discord server](https://discordapp.com/invite/s5SKBmY), [my forum](https://thenofraudsclub.proboards.com), or [my email](mailto:Pronogo@hotmail.com)

For prerelease builds, see [this repo](https://gitlab.com/Pr0nogo3/specialbus/-/tree/master).

For more info on Cosmonarchy BW, including a project introduction and primer, visit the project's [website](http://fraudsclub.com/cosmonarchy-bw). You can also follow development progress by consulting our [Trello board](https://trello.com/b/ZuOmqT8i/cosmonarchy).

Source code repos for the plugins used in Cosmonarchy are available in [our tools group](https://gitlab.com/the-no-frauds-club/tools).

# Current caveats:
**Occasional crashes** occur after multiple games in the same instance. The cause is unknown and the current workaround is to restart your instance after each game ends. Exceedingly long games (i.e. beyond 2 hours) can fall prey to instability regardless of how many games in the same instance.

**A rare freeze**, presumably AI-script related, occasionally occurs around 12 minutes. This will silently crash to desktop after a delay.

**Game performance** occasionally suffers while hundreds of AI-controlled units calculate pathing. This performance impact resolves when the paths are fully calculated.

**AI performance** needs improvement in certain cases and is being continually adjusted from patch to patch.

**Audiovisuals** for custom abilities are lacking, and are the target of all future patches.

# Setup:
If you do not have version 1.16.1 installed, [follow this guide](http://fraudsclub.com/starcraft/play/). You should also consult this guide if you encounter any issues with windowed mode, running mods, capturing with recording or streaming software, etc.

Place Cosmonarchy's maps in your maps folder, creating a specific directory for them if you wish.

Place Cosmonarchy's exe in the same directory as the included plugins folder. **Without the plugins folder being in the same directory, the exe will not run.** If it still fails to run despite this, try placing the plugins in your Starcraft installation directory.

Cosmonarchy's custom exe and/or the classic installer from FaRTy1billion may alert your antivirus. Starcraft mods use code injectors to launch the game normally before applying the mod's changes, which will always seem suspicious to antivirus software. This mod and FaRTy's installer are both safe for your PC, but feel free to reach out if you have any concerns.

If you run into any other issues or have any other questions after following the guide linked above, message me.

# Multiplayer:
Community games are organized using Radmin VPN, a free and seemingly lightweight network utility.

* Download [Radmin VPN](https://www.radmin-vpn.com/)
* Join the No Frauds Club network with the following credentials:
  * __Network name:__ FRAUDSCLUB
  * __Password:__ toddad69
* Create and join Starcraft games by launching Starcraft, selecting Multiplayer, and selecting Local Area Network (UDP)
* Join the [No Frauds Club discord server](https://discordapp.com/invite/s5SKBmY) to organize community games in real time

If your Radmin VPN is stuck on "waiting for adapter response", it is likely that the software's driver did not install correctly or was somehow corrupted. The only known fix for this is to reinstall the utility.
